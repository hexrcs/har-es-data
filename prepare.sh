#!/usr/bin/env bash

# Reassemble h5 dataset files

reassemble_file() {
    # Reassemble the file.
    cat "dataset/$1"* >"$1"
    shasum "$1"
    mv "$1" "output/$1"
}

# a list of file names
Files=(
    "daphnet.h5"
    "opportunity.h5"
    "pamap2.h5"
    "sphere.h5"
)

# If no argument, reassemble all files, else reassemble the file with the given name.
if [ $# -eq 0 ]; then
    for file in ${Files[*]}; do
        reassemble_file $file
    done
else
    reassemble_file "$1"
fi
